resource "aws_s3_bucket" "app-code" {
  bucket = "${var.prefix}-alb-log"
}

resource "aws_s3_bucket_versioning" "versioning_app-code" {
  bucket = aws_s3_bucket.app-code.id
  versioning_configuration {
    status = "Disabled"
  }
}


resource "aws_s3_bucket_ownership_controls" "s3appcodecontrol" {
  bucket = aws_s3_bucket.app-code.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "app-code" {
  depends_on = [aws_s3_bucket_ownership_controls.s3appcodecontrol]
  bucket     = aws_s3_bucket.app-code.id
  acl        = "private"
}


## terraform state


resource "aws_s3_bucket" "terraformtfstate" {
  bucket = "nsearch-terrafrom-state"
}

resource "aws_s3_bucket_ownership_controls" "s3terraformstatecontrol" {
  bucket = aws_s3_bucket.terraformtfstate.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "terrafromstates3acl" {
  depends_on = [aws_s3_bucket_ownership_controls.s3terraformstatecontrol]
  bucket     = aws_s3_bucket.terraformtfstate.id
  acl        = "private"
}

resource "aws_dynamodb_table" "tfstatelockid" {
  name         = "nsearch-tftstaelockid"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }
}