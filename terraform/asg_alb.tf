
##Get latest AWS Amazon Linux 2 Image

data "aws_ami" "amazon-linux-2" {
  most_recent = true

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}


#Create a Launch Template with user data
resource "aws_launch_template" "nsearchawslaunchtemplate" {
  name_prefix   = "nsearch-launch-template"
  image_id      = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"
  key_name      = "nsearch-assignment-ec2key"
  iam_instance_profile {
    name = "ec2-s3-role_profile"
  }
  vpc_security_group_ids = [aws_security_group.ec2securitygroup.id]
  user_data = base64encode(<<EOF
                #!/bin/bash
                sudo yum update -y
                sudo amazon-linux-extras install -y nginx1.12
                sudo service nginx start
                sudo aws s3 cp s3://nsearch-assignment-alb-log/incremental_decremental_counter.html /usr/share/nginx/html/index.html
                sudo aws s3 cp s3://nsearch-assignment-alb-log/incremental_decremental_counter.css /usr/share/nginx/html/
                sudo aws s3 cp s3://nsearch-assignment-alb-log/incremental_decremental_counter.js /usr/share/nginx/html/
                EOF
  )

}

resource "aws_autoscaling_group" "nsearch-asg" {
  # availability_zones = ["ap-southeast-1a"]
  desired_capacity    = 3
  max_size            = 5
  min_size            = 3
  vpc_zone_identifier = module.vpc.private_subnets
  launch_template {
    id      = aws_launch_template.nsearchawslaunchtemplate.id
    version = "$Latest"
  }
}


resource "aws_security_group" "ec2securitygroup" {
  name_prefix = "nsearch-ec2-security-group"
  vpc_id      = module.vpc.vpc_id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.cidr}"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.cidr}"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

locals {
  traffic_rules = {
    allow_http_all = {
      "80" = {
        cidr      = ["0.0.0.0/0"]
        ipv6_cidr = ["::/0"]
      }
    }
    allow_all = {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      description      = ""
      prefix_list_ids  = null
      security_groups  = []
      self             = false
    }
  }
}

resource "aws_security_group" "alb_nsearch" {
  name        = "${var.prefix}-alb"
  description = "Allow http and https inbound traffic"
  vpc_id      = module.vpc.vpc_id

  dynamic "ingress" {
    for_each = local.traffic_rules.allow_http_all
    content {
      from_port        = ingress.key
      to_port          = ingress.key
      cidr_blocks      = ingress.value.cidr
      protocol         = "tcp"
      ipv6_cidr_blocks = ingress.value.ipv6_cidr
      description      = ""
      prefix_list_ids  = null
      security_groups  = []
      self             = false
    }
  }

  egress = [local.traffic_rules.allow_all]

}

resource "aws_lb" "nsearch" {
  name                       = "${var.prefix}-lb"
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.alb_nsearch.id]
  subnets                    = module.vpc.public_subnets
  enable_deletion_protection = false
  access_logs {
    bucket  = aws_s3_bucket.app-code.bucket
    prefix  = "${var.prefix}-alb-nsearch"
    enabled = false
  }
}

resource "aws_lb_target_group" "nsearch" {
  name        = "${var.prefix}-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = module.vpc.vpc_id
  health_check {
    path                = "/"
    port                = 80
    healthy_threshold   = 6
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200-399" # has to be HTTP 200 or fails
  }
}

resource "aws_lb_listener" "web_80" {
  load_balancer_arn = aws_lb.nsearch.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nsearch.arn
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_nsearch" {
  autoscaling_group_name = aws_autoscaling_group.nsearch-asg.id
  lb_target_group_arn    = aws_lb_target_group.nsearch.arn

}