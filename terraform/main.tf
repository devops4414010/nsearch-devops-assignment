provider "aws" {
  region  = "ap-southeast-1"
  profile = "default"
  default_tags {
    tags = {
      Environment     = "test"
      Provisioner     = "terrafrom"
      ApplicationName = "Nsearch-assignment"
    }
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.58.0"
    }
  }

  backend "s3" {
    bucket         = "nsearch-terrafrom-state"
    key            = "terraform.tfstate"
    region         = "ap-southeast-1"
    dynamodb_table = "nsearch-tftstaelockid"
  }
}