resource "aws_iam_instance_profile" "IAM_profile" {
  name = "ec2-s3-role_profile"
  role = aws_iam_role.ec2-s3-role.name
}

# Create an IAM policy for S3 access
resource "aws_iam_policy" "s3-access" {
  name = "s3-access-policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "s3:Get*",
          "s3:List*",
          "s3:Put*",
          "s3:Delete*",
        ]
        Resource = "arn:aws:s3:::nsearch-assignment-alb-log/*"
      }
    ]
  })
}

# Create an IAM role to be attached to EC2 instances
resource "aws_iam_role" "ec2-s3-role" {
  name = "ec2-s3-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

# Attach the S3 access policy to the IAM role
resource "aws_iam_role_policy_attachment" "s3-access-attachment" {
  policy_arn = aws_iam_policy.s3-access.arn
  role       = aws_iam_role.ec2-s3-role.name
}