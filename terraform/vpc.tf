data "aws_availability_zones" "available" {
  state = "available"
}


locals {
  azs = data.aws_availability_zones.available.names
}


module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.11"

  name = "${var.prefix}-vpc"
  cidr = var.cidr

  azs              = local.azs
  private_subnets  = var.private_subnets
  public_subnets   = var.public_subnets
  database_subnets = var.database_subnets

  enable_vpn_gateway     = false
  one_nat_gateway_per_az = false
  enable_nat_gateway     = true
  single_nat_gateway     = true

  create_database_subnet_group           = true
  create_database_subnet_route_table     = false
  create_database_internet_gateway_route = false

  enable_dns_hostnames = true
  enable_dns_support   = true

}