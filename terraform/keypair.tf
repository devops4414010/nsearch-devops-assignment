resource "tls_private_key" "ec2keyapir" {
  algorithm = "RSA"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name   = "${var.prefix}-ec2key"
  public_key = tls_private_key.ec2keyapir.public_key_openssh
}