variable "aws_region" { default = "ap-southeast-1" }
variable "prefix" { default = "nsearch-assignment" }
variable "environment" { default = "dev" }

variable "cidr" { default = "10.99.0.0/16" }

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)
  default     = ["10.99.1.0/24", "10.99.2.0/24"]
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)
  default     = ["10.99.11.0/24", "10.99.12.0/24"]
}


variable "database_subnets" {
  description = "A list of database subnets inside the VPC"
  type        = list(string)
  default     = ["10.99.21.0/24", "10.99.22.0/24"]
}

variable "db_public_access" {
  default = false
}

variable "db_multi_az" {
  default = true
}