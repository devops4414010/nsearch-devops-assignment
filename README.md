# Nsearch DevOps Technical Assignment Execution Summary

This is a README file to walk through on 2 assigned tasks implementation

## Table of Contents


- [Pre-requisites](#pre-requisites)
- [Scenario1](#scenario1)
- [CICD](#cicd)
- [Scenario2](#scenario2)
- [Prometheus-Grafana](#prometheus)
- [Snapshots](#evidence)

## Pre-requisites

1. Clone the repository from default main branch
2. Install the required docker and docker-compose on testing machine
3. Access to GitLab portal for read only access and access to Docker hub to download docker images
4. Local setup of Minikube to deploy containers
5. Setup AWS default profile
6. Created S3 bucket for Terraform state to store
7. Install latest terraform package

## scenario1


1. Restrict inbound access to both public ALB and Server Fleet A to only allow on port
   80/TCP

Terraform snippet used to achieve tgis
```
resource "aws_security_group" "ec2securitygroup" {
  name_prefix = "nsearch-ec2-security-group"
  vpc_id      = module.vpc.vpc_id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.cidr}"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.cidr}"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

```

```
locals {
  traffic_rules = {
    allow_http_all = {
      "80" = {
        cidr      = ["0.0.0.0/0"]
        ipv6_cidr = ["::/0"]
      }
    }
    allow_all = {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      description      = ""
      prefix_list_ids  = null
      security_groups  = []
      self             = false
    }
  }
}

resource "aws_security_group" "alb_nsearch" {
  name        = "${var.prefix}-alb"
  description = "Allow http and https inbound traffic"
  vpc_id      = module.vpc.vpc_id

  dynamic "ingress" {
    for_each = local.traffic_rules.allow_http_all
    content {
      from_port        = ingress.key
      to_port          = ingress.key
      cidr_blocks      = ingress.value.cidr
      protocol         = "tcp"
      ipv6_cidr_blocks = ingress.value.ipv6_cidr
      description      = ""
      prefix_list_ids  = null
      security_groups  = []
      self             = false
    }
  }

  egress = [local.traffic_rules.allow_all]

}
```
2. Install Nginx on Amazon Linux 2, Server Fleet A of EC2

```
##Get latest AWS Amazon Linux 2 Image

data "aws_ami" "amazon-linux-2" {
  most_recent = true

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}


#Create a Launch Template with user data
resource "aws_launch_template" "nsearchawslaunchtemplate" {
  name_prefix   = "nsearch-launch-template"
  image_id      = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"
  key_name      = "nsearch-assignment-ec2key"
  iam_instance_profile {
    name = "ec2-s3-role_profile"
  }
  vpc_security_group_ids = [aws_security_group.ec2securitygroup.id]
  user_data = base64encode(<<EOF
                #!/bin/bash
                sudo yum update -y
                sudo amazon-linux-extras install -y nginx1.12
                sudo service nginx start
                sudo aws s3 cp s3://nsearch-assignment-alb-log/incremental_decremental_counter.html /usr/share/nginx/html/index.html
                sudo aws s3 cp s3://nsearch-assignment-alb-log/incremental_decremental_counter.css /usr/share/nginx/html/
                sudo aws s3 cp s3://nsearch-assignment-alb-log/incremental_decremental_counter.js /usr/share/nginx/html/
                EOF
  )

}

resource "aws_autoscaling_group" "nsearch-asg" {
  # availability_zones = ["ap-southeast-1a"]
  desired_capacity    = 3
  max_size            = 5
  min_size            = 3
  vpc_zone_identifier = module.vpc.private_subnets
  launch_template {
    id      = aws_launch_template.nsearchawslaunchtemplate.id
    version = "$Latest"
  }
}

```
3. Able to view Incremental and Decremental Counter webpage from public internet
   served by Server Fleet A (Screenshot required for both the original page and
   modified uppercase text page)

   ![PublicAccess](./snapshots/scenario_1/4.elb_endpoint_app_initial.png)
   ![PublicAccess2](./snapshots/scenario_1/10.elb_endpoint_app_CAPITALS.png)
  
ASG Instance Refresh:

![ASGRefresh2](./snapshots/scenario_1/8.asg_Instance_refresh.png)


All other snapshots 

https://gitlab.com/devops4414010/nsearch-devops-assignment/-/tree/main/snapshots/scenario_1?ref_type=heads


## cicd

CICD Pipeline to deploy an updated app code into S3 bucket through Gitlab pipelines

https://gitlab.com/devops4414010/nsearch-devops-assignment/-/pipelines

![CICD](./snapshots/scenario_1/6.cicd/1.cicd.png)
![CICD](./snapshots/scenario_1/6.cicd/2.cicd_S3_deploy_job.png)
![CICD](./snapshots/scenario_1/6.cicd/6.CAPITAL_code_merge.png)


4. Describe in a README file, what further improvements to above architecture and
   setup would you recommend and why.


1. Prefer to deploy into dedicated VPC instead of default VPC as we can enforce least access IAM policies, SecurityGroups deny all for subnets
2. Multi AZ setup for HA
3. Implement Secure SSL cert access to endpoint
4. Integrate AWS WAF for endpoint protection
5. Create AWS S3 private endpoint to reduce data transfer pricing form private subnets into to public
6. Enable VPC flow logs and AWS CloudTrail 

## scenario2

1. Created docker image as follows and pushed my public repository

```
FROM nginx:stable-alpine-slim
COPY app/ /usr/share/nginx/html/
RUN mv /usr/share/nginx/html/incremental_decremental_counter.html /usr/share/nginx/html/index.html
CMD ["nginx", "-g", "daemon off;"]

```

```
bhaskar@Bhaskars-MacBook-Air nsearch-devops-assignment % docker build . -t bhaskaryasam/nsearch_nginx:latest
[+] Building 4.1s (8/8) FINISHED                                                                                                                                                                                
 => [internal] load build definition from Dockerfile                                                                                                                                                       0.1s
 => => transferring dockerfile: 74B                                                                                                                                                                        0.0s
 => [internal] load .dockerignore                                                                                                                                                                          0.0s
 => => transferring context: 2B                                                                                                                                                                            0.0s
 => [internal] load metadata for docker.io/library/nginx:stable-alpine-slim                                                                                                                                3.2s
 => [internal] load build context                                                                                                                                                                          0.0s
 => => transferring context: 2.23kB                                                                                                                                                                        0.0s
 => CACHED [1/3] FROM docker.io/library/nginx:stable-alpine-slim@sha256:c27794c4ae552c92541ec338692b369f83409958c5fcea00ab48b848ab7e46b6                                                                   0.0s
 => [2/3] COPY app/ /usr/share/nginx/html/                                                                                                                                                                 0.1s
 => [3/3] RUN mv /usr/share/nginx/html/incremental_decremental_counter.html /usr/share/nginx/html/index.html                                                                                               0.4s
 => exporting to image                                                                                                                                                                                     0.2s
 => => exporting layers                                                                                                                                                                                    0.0s
 => => writing image sha256:ef7fd0bb3efe3e0305bea5a3d2d3e381be4c8221d0b6a8ebe1446f4486dcd461                                                                                                               0.0s
 => => naming to docker.io/bhaskaryasam/nsearch_nginx:latest                                                                                                                                               0.0s
bhaskar@Bhaskars-MacBook-Air nsearch-devops-assignment % docker images
'REPOSITORY                    TAG       IMAGE ID       CREATED          SIZE
bhaskaryasam/nsearch_nginx    latest    ef7fd0bb3efe   37 seconds ago   12MB
```


2. Make use of scenario 1 code and display your <your first name> using an
   environment variable supplied to the container.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nserach-nginx-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nsearch_nginx
  template:
    metadata:
      labels:
        app: nsearch_nginx
    spec:
      containers:
      - name: nserach-nginx-container
        image: bhaskaryasam/nsearch_nginx:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 80
        command: ["/bin/sh", "-c", "echo $MY_NAME >> /usr/share/nginx/html/index.html && nginx -g 'daemon off;'"]
        env:
          - name: MY_NAME
            value: "BHASKAR"
          - name: DB_HOST
            value: mysql
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: mysql-credentials
                key: username
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: mysql-credentials
                key: password
---
apiVersion: v1
kind: Service
metadata:
  name: nserach-nginx-service
spec:
  selector:
    app: nsearch_nginx
  ports:
  - name: http
    port: 8888
    targetPort: 80
  type: NodePort
---
```
Screenshot:
![MINIKUBE](./snapshots/scenario_2/2.Access_Minikube_service_endpoint.png)
![MINIKUBE](./snapshots/scenario_2/1.kubenetes_deployment_with_ENV_Value.png)


2. Ensure that the Incremental and Decremental Counter is able to connect to Mysql
   server. No code change is required from scenario 1.

Above app deployment already integarted with mysql service endpoint with use name and password, here is mysql manifest used to create mysql setup in same namespace

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: my-pv
spec:
  storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: /var/lib/docker/volumes/mysql

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi

---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mysql
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
        - name: mysql
          image: mysql:latest
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                   name: mysql-credentials
                   key: rootpassword
            - name: MYSQL_DATABASE
              value: mydb
            - name: MYSQL_USER
              valueFrom:
                secretKeyRef:
                   name: mysql-credentials
                   key: username
            - name: MYSQL_PASSWORD
              valueFrom:
                secretKeyRef:
                   name: mysql-credentials
                   key: password
          ports:
            - containerPort: 3306
          volumeMounts:
            - name: mysql-persistent-storage
              mountPath: /var/lib/mysql
      volumes:
        - name: mysql-persistent-storage
          hostPath:
            path: /var/lib/docker/volumes/mysqltest
---

apiVersion: v1
kind: Service
metadata:
  name: mysql
  labels:
    app: mysql
spec:
  selector:
    app: mysql
  ports:
    - name: mysql-port
      protocol: TCP
      port: 3306
      targetPort: 3306

---

apiVersion: v1
kind: Secret
metadata:
  name: mysql-credentials
type: Opaque
data:
  username: <user_name>
  password: <password>
  rootpassword: <root_password>

---

```


## prometheus

1. Monitor the resources using Prometheus and Grafana

Deployed Prometheus and Grafana using below script in local Minikube setup

```
#!/bin/bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus
helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana
minikube service grafana --url

```

Deployment of Prometheus

![PROMETHEUS](./snapshots/scenario_2/prometheus/1.Prometheus_Deployment.png)
![GRAFANA](./snapshots/scenario_2/prometheus/2.Grafana_Deployment.png)
![DEPLOYMENTSERVICES](./snapshots/scenario_2/prometheus/3.Prometheus_Grafana_deployment.png)
![GRAFANALOGIN](./snapshots/scenario_2/prometheus/4.Gafana_Login.png)
![GRAFANAPROMETHEUSINTEGRATION](./snapshots/scenario_2/prometheus/5.Grafan_datasource_config.png)
![GRAFANAPROMETHEUSINTEGRATION](./snapshots/scenario_2/prometheus/6.Prometheus_integrate.png)
![METRICS](./snapshots/scenario_2/prometheus/7.Minikube_metrics.png)
![METRICS](./snapshots/scenario_2/prometheus/8.Nsearch_deployment_prods_1.png)
![METRICS](./snapshots/scenario_2/prometheus/9.Nsearch_deployment_prods_2.png)
![METRICS](./snapshots/scenario_2/prometheus/10.Nsearch_deployment_prods_3.png)

## evidence

https://gitlab.com/devops4414010/nsearch-devops-assignment/-/tree/main/snapshots


